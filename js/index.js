"use strict";

const DATA = [
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m1.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "8 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f1.png",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m2.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Тетяна",
    "last name": "Мороз",
    photo: "./img/trainers/trainer-f2.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
  },
  {
    "first name": "Сергій",
    "last name": "Коваленко",
    photo: "./img/trainers/trainer-m3.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
  },
  {
    "first name": "Олена",
    "last name": "Лисенко",
    photo: "./img/trainers/trainer-f3.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
  },
  {
    "first name": "Андрій",
    "last name": "Волков",
    photo: "./img/trainers/trainer-m4.jpg",
    specialization: "Бійцівський клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
  },
  {
    "first name": "Наталія",
    "last name": "Романенко",
    photo: "./img/trainers/trainer-f4.jpg",
    specialization: "Дитячий клуб",
    category: "спеціаліст",
    experience: "3 роки",
    description:
      "Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
  },
  {
    "first name": "Віталій",
    "last name": "Козлов",
    photo: "./img/trainers/trainer-m5.jpg",
    specialization: "Тренажерний зал",
    category: "майстер",
    experience: "10 років",
    description:
      "Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
  },
  {
    "first name": "Юлія",
    "last name": "Кравченко",
    photo: "./img/trainers/trainer-f5.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "4 роки",
    description:
      "Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
  },
  {
    "first name": "Олег",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-m6.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "12 років",
    description:
      "Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
  },
  {
    "first name": "Лідія",
    "last name": "Попова",
    photo: "./img/trainers/trainer-f6.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
  },
  {
    "first name": "Роман",
    "last name": "Семенов",
    photo: "./img/trainers/trainer-m7.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
  },
  {
    "first name": "Анастасія",
    "last name": "Гончарова",
    photo: "./img/trainers/trainer-f7.jpg",
    specialization: "Басейн",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
  },
  {
    "first name": "Валентин",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-m8.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
  },
  {
    "first name": "Лариса",
    "last name": "Петренко",
    photo: "./img/trainers/trainer-f8.jpg",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "7 років",
    description:
      "Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
  },
  {
    "first name": "Олексій",
    "last name": "Петров",
    photo: "./img/trainers/trainer-m9.jpg",
    specialization: "Басейн",
    category: "майстер",
    experience: "11 років",
    description:
      "Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
  },
  {
    "first name": "Марина",
    "last name": "Іванова",
    photo: "./img/trainers/trainer-f9.jpg",
    specialization: "Тренажерний зал",
    category: "спеціаліст",
    experience: "2 роки",
    description:
      "Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
  },
  {
    "first name": "Ігор",
    "last name": "Сидоренко",
    photo: "./img/trainers/trainer-m10.jpg",
    specialization: "Дитячий клуб",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
  },
  {
    "first name": "Наталія",
    "last name": "Бондаренко",
    photo: "../img/trainers/trainer-f10.jpg",
    specialization: "Бійцівський клуб",
    category: "майстер",
    experience: "8 років",
    description:
      "Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
  },
  {
    "first name": "Андрій",
    "last name": "Семенов",
    photo: "../img/trainers/trainer-m11.jpg",
    specialization: "Тренажерний зал",
    category: "інструктор",
    experience: "1 рік",
    description:
      "Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
  },
  {
    "first name": "Софія",
    "last name": "Мельник",
    photo: "./img/trainers/trainer-f11.jpg",
    specialization: "Басейн",
    category: "спеціаліст",
    experience: "6 років",
    description:
      "Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
  },
  {
    "first name": "Дмитро",
    "last name": "Ковальчук",
    photo: "./img/trainers/trainer-m12.png",
    specialization: "Дитячий клуб",
    category: "майстер",
    experience: "10 років",
    description:
      "Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
  },
  {
    "first name": "Олена",
    "last name": "Ткаченко",
    photo: "./img/trainers/trainer-f12.jpg",
    specialization: "Бійцівський клуб",
    category: "спеціаліст",
    experience: "5 років",
    description:
      "Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
  },
];

document.addEventListener("DOMContentLoaded", () => {
  const container = document.querySelector(".trainers-cards__container");
  const filtersSection = document.querySelector(".filters");
  const sortingSection = document.querySelector(".sorting");
  const sortByNameButton = document.querySelectorAll(".sorting__btn")[1];
  const sortByExperienceButton = document.querySelectorAll(".sorting__btn")[2];
  const showButton = document.querySelector(".filters__submit");
  const sidebarAside = document.querySelectorAll(".sidebar");

  let currentData = DATA.slice();
  sidebarAside.forEach((aside) => {
    aside.hidden = false;
  });

  function displayTrainers(data) {
    container.innerHTML = "";
    data.forEach((trainer) => {
      const cardTemplate = document
        .getElementById("trainer-card")
        .content.cloneNode(true);
      const trainerPhoto = cardTemplate.querySelector(".trainer__img");
      const trainerName = cardTemplate.querySelector(".trainer__name");
      const showDetailsButton = cardTemplate.querySelector(
        ".trainer__show-more"
      );

      if (trainerPhoto) {
        trainerPhoto.src = trainer.photo;
        trainerPhoto.alt = `${trainer["first name"]} ${trainer["last name"]}`;
      }
      if (trainerName) {
        trainerName.textContent = `${trainer["first name"]} ${trainer["last name"]}`;
      }
      if (showDetailsButton) {
        showDetailsButton.addEventListener("click", () => showModal(trainer));
      }
      container.appendChild(cardTemplate);
    });
  }

  function showModal(trainer) {
    const modalTemplate = document
      .getElementById("modal-template")
      .content.cloneNode(true);
    const modal = modalTemplate.querySelector(".modal");
    const modalPhoto = modal.querySelector(".modal__img");
    const modalName = modal.querySelector(".modal__name");
    const modalSpecialization = modal.querySelector(
      ".modal__point--specialization"
    );
    const modalCategory = modal.querySelector(".modal__point--category");
    const modalExperience = modal.querySelector(".modal__point--experience");
    const modalDescription = modal.querySelector(".modal__text");
    const closeButton = modal.querySelector(".modal__close");

    if (modalPhoto) {
      modalPhoto.src = trainer.photo;
      modalPhoto.alt = `${trainer["first name"]} ${trainer["last name"]}`;
    }
    if (modalName) {
      modalName.textContent = `${trainer["first name"]} ${trainer["last name"]}`;
    }
    if (modalSpecialization) {
      modalSpecialization.textContent = `Напрям тренера: ${trainer.specialization}`;
    }
    if (modalCategory) {
      modalCategory.textContent = `Категорія: ${trainer.category}`;
    }
    if (modalExperience) {
      modalExperience.textContent = `Досвід: ${trainer.experience}`;
    }
    if (modalDescription) {
      modalDescription.textContent = trainer.description;
    }
    if (closeButton) {
      closeButton.addEventListener("click", () => {
        document.body.removeChild(modal);
        document.body.style.overflow = "auto";
      });
    }
    document.body.appendChild(modal);
    modal.style.display = "block";
    document.body.style.overflow = "hidden";
  }

  function sortTrainers(key) {
    const sortedTrainers = [...currentData].sort((a, b) => {
      if (key === "last name") {
        return a[key].localeCompare(b[key]);
      } else if (key === "experience") {
        const experienceA = parseInt(a[key]);
        const experienceB = parseInt(b[key]);
        return experienceB - experienceA;
      }
    });
    displayTrainers(sortedTrainers);
  }

  document.querySelectorAll(".sorting__btn").forEach((btn) => {
    btn.addEventListener("click", () => {
      document
        .querySelector(".sorting__btn--active")
        .classList.remove("sorting__btn--active");
      btn.classList.add("sorting__btn--active");
      if (btn.textContent.includes("замовчуванням")) {
        displayTrainers(DATA);
      } else if (btn.textContent.includes("за прізвищем")) {
        sortTrainers("last name");
      } else if (btn.textContent.includes("за досвідом")) {
        sortTrainers("experience");
      }
    });
  });

  function getSelectedValue(name) {
    return document.querySelector(`input[name="${name}"]:checked`).value;
  }

  function matchesFilter(value, filter) {
    return filter === "all" || value === filter;
  }

  function filterTrainers() {
    const direction = getSelectedValue("direction");
    const category = getSelectedValue("category");
    let selectredDerection = "";
    let selectedCategory = "";
    if (direction == "swimming pool") {
      selectredDerection = "Басейн";
    }
    if (direction == "gym") {
      selectredDerection = "Тренажерний зал";
    }
    if (direction == "fight club") {
      selectredDerection = "Бійцівський клуб";
    }
    if (direction == "kids club") {
      selectredDerection = "Дитячий клуб";
    }
    if (category == "instructor") {
      selectedCategory = "інструктор";
    }
    if (category == "specialist") {
      selectedCategory = "спеціаліст";
    }
    if (category == "master") {
      selectedCategory = "майстер";
    }

    currentData = DATA.filter((trainer) => {
      const matchesDirection = matchesFilter(
        trainer.specialization,
        selectredDerection
      );
      const matchesCategory = matchesFilter(trainer.category, selectedCategory);
      return matchesDirection && matchesCategory;
    });

    console.log(currentData);
    displayTrainers(currentData);
  }

  displayTrainers(currentData);
  if (filtersSection) {
    filtersSection.hidden = false;
  }
  if (sortingSection) {
    sortingSection.hidden = false;
  }

  sortByNameButton.addEventListener("click", () => sortTrainers("last name"));
  sortByExperienceButton.addEventListener("click", () =>
    sortTrainers("experience")
  );
  showButton.addEventListener("click", (event) => {
    event.preventDefault();
    filterTrainers();
  });
});
